package VentanasGUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import VentanasGUI.OpcionesGUI.OpcionesAlumno;
import VentanasGUI.OpcionesGUI.OpcionesApoderado;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Opciones extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Opciones frame = new Opciones();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Opciones() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnMatricularAlumno = new JButton("Opciones Estudiante");
		btnMatricularAlumno.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				OpcionesAlumno ma = new OpcionesAlumno();
				ma.setVisible(true);
				dispose();
			}
		});
		btnMatricularAlumno.setBounds(106, 79, 224, 23);
		contentPane.add(btnMatricularAlumno);
		
		JButton btnOpcionesProfesor = new JButton("Opciones Profesor");
		btnOpcionesProfesor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnOpcionesProfesor.setBounds(106, 113, 224, 23);
		contentPane.add(btnOpcionesProfesor);
		
		JButton btnOpcionesApoderado = new JButton("Opciones Apoderado");
		btnOpcionesApoderado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OpcionesApoderado oa = new OpcionesApoderado();
				oa.setVisible(true);
				dispose();
			}
		});
		btnOpcionesApoderado.setBounds(106, 147, 224, 23);
		contentPane.add(btnOpcionesApoderado);
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Admin a = new Admin();
				a.setVisible(true);
				dispose();
			}
		});
		btnVolver.setBounds(174, 227, 89, 23);
		contentPane.add(btnVolver);
	}

}
