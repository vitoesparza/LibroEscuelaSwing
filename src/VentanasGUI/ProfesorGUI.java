package VentanasGUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.xml.transform.TransformerException;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;

import Datos.reportes.profesor.ReporteEstudiantes;
import VentanasGUI.ProfesorGUIs.IngresarAnotacionGUI;
import VentanasGUI.ProfesorGUIs.IngresarNotaGUI;
import VentanasGUI.ProfesorGUIs.IngresarPlanGUI;
import libropackage.Asignatura;
import libropackage.CursoDAO;

import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class ProfesorGUI extends JFrame {

	private JPanel contentPane;
	private JLabel jlprofesor = new JLabel("");
	private JComboBox cbAsignatura = new JComboBox();
	private ArrayList<libropackage.Estudiante> alumnoObject = new ArrayList();
	private ArrayList<String>idAlumno= new ArrayList();
	private int idProfesor;
	private int idcurso;
	private int idasignatura;
	private String nivel;
	private String letra;
	private String nombreProfesor;
	private ArrayList<libropackage.Asignatura>asigObject= new ArrayList<>();
	private static final int ROW_COUNT = 1000;
	private boolean error = false;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ProfesorGUI frame = new ProfesorGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ProfesorGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 530, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnIngresarNotas = new JButton("Ingresar Notas");
		btnIngresarNotas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				IngresarNotaGUI ing = new IngresarNotaGUI();
				ing.setVisible(true);
				try {
					seleccionado();
					ing.estudiantes(idcurso,idProfesor,idasignatura,nombreProfesor);
				} catch (PersistentException e) {
					e.printStackTrace();
				}
				dispose();
			}
		});
		btnIngresarNotas.setBounds(46, 81, 201, 23);
		contentPane.add(btnIngresarNotas);
		
		JButton btnIngresarAnotaciones = new JButton("Ingresar Anotaciones");
		btnIngresarAnotaciones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				IngresarAnotacionGUI iag = new IngresarAnotacionGUI();
				try {
					seleccionado();
					iag.anotacion(idcurso, idProfesor);
				} catch (PersistentException e) {
					e.printStackTrace();
				}
				iag.setVisible(true);
				dispose();
			}
		});
		btnIngresarAnotaciones.setBounds(46, 115, 201, 23);
		contentPane.add(btnIngresarAnotaciones);
		
		JButton btnIngresarPlanificaciones = new JButton("Ingresar Planificaciones");
		btnIngresarPlanificaciones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ingresarPlanificaciones();
			}
		});
		btnIngresarPlanificaciones.setBounds(46, 149, 201, 23);
		contentPane.add(btnIngresarPlanificaciones);
		
		jlprofesor.setBounds(46, 35, 158, 14);
		contentPane.add(jlprofesor);
		
		JButton btnIngresarAsistencias = new JButton("Ingresar Asistencias");
		btnIngresarAsistencias.setBounds(46, 183, 201, 23);
		contentPane.add(btnIngresarAsistencias);
		
		JButton btnGenerarReportePromedios = new JButton("Reporte Promedios");
		btnGenerarReportePromedios.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					seleccionado();
					rPromedios(idcurso, idProfesor, idasignatura);
				} catch (TransformerException e) {
					e.printStackTrace();
				} catch (PersistentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnGenerarReportePromedios.setBounds(292, 81, 201, 23);
		contentPane.add(btnGenerarReportePromedios);
		
		
		cbAsignatura.setModel(new DefaultComboBoxModel(new String[] {}));
		cbAsignatura.setBounds(335, 29, 158, 20);
		contentPane.add(cbAsignatura);
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Admin a = new Admin();
				a.setVisible(true);
				dispose();
			}
		});
		btnVolver.setBounds(46, 227, 99, 23);
		contentPane.add(btnVolver);
	}
	
	public void profesor(int idProfe)throws PersistentException{
		try {
			PersistentTransaction t = libropackage.LibroClasePersistentManager.instance().getSession().beginTransaction();
			try {
				libropackage.Profesor profesor = libropackage.ProfesorDAO.getProfesorByORMID(idProfe);
				//profesor.getId_pk();
				jlprofesor.setText(profesor.getNombre() +" "+ profesor.getApellido());
				nombreProfesor=profesor.getNombre() +" "+ profesor.getApellido();
				asignaturasImp(idProfe);
				idProfesor=idProfe;
				t.commit();
			}
			catch (Exception e) {
				t.rollback();
				JOptionPane.showMessageDialog(null, "Profesor no Existe");
				error=true;
			}
		}
		finally {
			libropackage.LibroClasePersistentManager.instance().disposePersistentManager();
		}
	}
	/**
	 * asignaturasImp (nombre corto para asignaturas impartidas)este m�todo llena la ComboBox 
	 * con la asignatura y el curso que imparte el profesor
	 * @param id es el id del profesor que es mandado desde el m�todo profesor
	 * @throws PersistentException
	 */
	public void asignaturasImp (int id) throws PersistentException{
		libropackage.Asignatura[] asignatura = libropackage.AsignaturaDAO.listAsignaturaByQuery(null, null);
		int length = Math.min(asignatura.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			if(Integer.parseInt(String.valueOf(asignatura[i].getProfesorid_fk()))==id){
				cbAsignatura.addItem(asignatura[i].getMateria()+" "
						+ ""+cursos(Integer.parseInt(String.valueOf(asignatura[i].getCurso_id_fk()))));
				asigObject.add(asignatura[i]);
			}
		}
	}
	/**
	 * este m�todo se encarga de buscar el curso perteneciente al profesor seleccionado
	 * @param id, es el id del curso enviado desde el m�todo asignaturasImp
	 * @return
	 * @throws PersistentException
	 */
	public String cursos (int id) throws PersistentException{
		String cursoSel = "";
		libropackage.Curso[] curso = libropackage.CursoDAO.listCursoByQuery(null, null);
		int length = Math.min(curso.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			if(Integer.parseInt(String.valueOf(curso[i].getId_pk()))==id){
				cursoSel=curso[i].getNivel()+" "+curso[i].getLetra();
			}
		}
		return cursoSel;
	}
	
	public void seleccionado () throws PersistentException{
		String cursel = (String)cbAsignatura.getSelectedItem();
		idasignatura=asigObject.get(cbAsignatura.getSelectedIndex()).getId_pk();
		nivel=cursel.substring(cursel.length()-3,cursel.length()-2);
		letra=cursel.substring(cursel.length()-1,cursel.length());
		libropackage.Curso[] curso = libropackage.CursoDAO.listCursoByQuery(null, null);
		int length = Math.min(curso.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			if(curso[i].getNivel().equals(Integer.parseInt(nivel)) && curso[i].getLetra().equals(letra) ){
				idcurso=curso[i].getId_pk();
			}
		}
	}
	
	private void rPromedios(int idCurso, int idProfesor,int idAsig) throws TransformerException {
		ReporteEstudiantes re = new ReporteEstudiantes();
		try {
			libropackage.Estudiante[] alumnos = libropackage.EstudianteDAO.listEstudianteByQuery(null, null);
			int length = Math.min(alumnos.length, ROW_COUNT);
			
			for (int i = 0; i < length; i++) {
				//System.out.println(alumnos[i].getCursoid_fk());
				//System.out.println(idCurso);
				if(String.valueOf(alumnos[i].getCursoid_fk()).equals(String.valueOf(idCurso))){
					idAlumno.add(""+alumnos[i].getId_pk());
				}
			}
			
			re.generarDocument(idAlumno,idProfesor,idAsig);
			re.generarXML();
		} catch (PersistentException e) {
			e.printStackTrace();
		}
		
	}
	
	public void ingresarPlanificaciones(){
		IngresarPlanGUI ipg = new IngresarPlanGUI();
		try {
			seleccionado();
			ipg.planificacion(idasignatura, idProfesor, idcurso);
			ipg.editPlan(idProfesor, idasignatura);
		} catch (PersistentException e) {
			e.printStackTrace();
		}
		ipg.setVisible(true);
		dispose();
	}
	
	public void volverByError(){
		if(error){
			Admin a = new Admin();
			a.setVisible(true);
			dispose();
		}
	}
}