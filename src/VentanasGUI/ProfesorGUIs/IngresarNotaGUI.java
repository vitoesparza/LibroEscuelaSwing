package VentanasGUI.ProfesorGUIs;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.orm.PersistentException;

import RunLESPackage.Conexion;
import VentanasGUI.ProfesorGUI;
import libropackage.Curso;
import libropackage.CursoDAO;

import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.SocketOption;

public class IngresarNotaGUI extends JFrame {

	private JPanel contentPane;
	private JTextField tfnotaIngresar;
	private JComboBox cbEstudiantes = new JComboBox();
	private JComboBox cbNotas = new JComboBox();
	private JLabel jlCurso = new JLabel("");
	private JLabel jlnomprofe = new JLabel("");
	private static final int ROW_COUNT = 1000;
	private int idProfe;
	private int idCursoGlobal;
	private int idAsign;
	private String nombreP;
	private ArrayList<libropackage.Estudiante> alumnoObject = new ArrayList();
	private ArrayList<libropackage.Asignatura> asigObject = new ArrayList();
	private ArrayList<Integer> idNotasArray = new ArrayList<>();
	private JTextField tfNotasEditar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					IngresarNotaGUI frame = new IngresarNotaGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public IngresarNotaGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 360);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		cbEstudiantes.setBounds(61, 40, 164, 20);
		contentPane.add(cbEstudiantes);

		tfnotaIngresar = new JTextField();
		tfnotaIngresar.setHorizontalAlignment(SwingConstants.CENTER);
		tfnotaIngresar.setBounds(225, 89, 86, 20);
		contentPane.add(tfnotaIngresar);
		tfnotaIngresar.setColumns(10);

		JLabel lblNota = new JLabel("Nota");
		lblNota.setBounds(61, 92, 86, 14);
		contentPane.add(lblNota);

		JButton btnAceptar = new JButton("Ingresar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				guardar();
			}
		});
		btnAceptar.setBounds(225, 120, 89, 23);
		contentPane.add(btnAceptar);

		JLabel lblEstudiantes = new JLabel(" Estudiantes");
		lblEstudiantes.setBounds(61, 15, 138, 14);
		contentPane.add(lblEstudiantes);

		JButton btnVolver = new JButton("Volver");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ProfesorGUI pg = new ProfesorGUI();
				try {
					pg.profesor(idProfe);
				} catch (PersistentException e1) {
					e1.printStackTrace();
				}
				pg.setVisible(true);
				dispose();
			}
		});
		btnVolver.setBounds(61, 282, 86, 23);
		contentPane.add(btnVolver);
		jlnomprofe.setHorizontalAlignment(SwingConstants.CENTER);

		jlnomprofe.setBounds(295, 291, 129, 14);
		contentPane.add(jlnomprofe);

		jlCurso.setHorizontalAlignment(SwingConstants.CENTER);
		jlCurso.setBounds(228, 15, 86, 14);
		contentPane.add(jlCurso);

		JLabel lblProfesor = new JLabel("Profesor");
		lblProfesor.setHorizontalAlignment(SwingConstants.CENTER);
		lblProfesor.setBounds(314, 266, 88, 14);
		contentPane.add(lblProfesor);

		cbEstudiantes.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					notasActuales();
				}

			}
		});
		
		
		cbNotas.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					tfNotasEditar.setText(String.valueOf(cbNotas.getSelectedItem()));
				}

			}
		});
		
		cbNotas.setBounds(61, 187, 86, 20);
		contentPane.add(cbNotas);

		JButton btnEditar = new JButton("Editar");
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				editar();
			}
		});
		btnEditar.setBounds(225, 217, 89, 23);
		contentPane.add(btnEditar);

		tfNotasEditar = new JTextField();
		tfNotasEditar.setBounds(61, 218, 86, 20);
		contentPane.add(tfNotasEditar);
		tfNotasEditar.setColumns(10);

		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				eliminarNota();
			}
		});
		btnEliminar.setBounds(225, 186, 89, 23);
		contentPane.add(btnEliminar);
	}

	public void estudiantes(int idCurso, int idprofesor, int idAsignatura, String nombreProfesor)
			throws PersistentException {

		libropackage.Curso curso = CursoDAO.getCursoByORMID(idCurso);
		libropackage.Estudiante[] alumnos = libropackage.EstudianteDAO.listEstudianteByQuery(null, null);
		int length = Math.min(alumnos.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			if (String.valueOf(alumnos[i].getCursoid_fk()).equals(String.valueOf(idCurso))) {
				cbEstudiantes.addItem(alumnos[i].getNombre() + " " + alumnos[i].getApellido());
				alumnoObject.add(alumnos[i]);
			}
		}
		// se obtiene la asignatura y se guarda en un arraylis para luego
		// utilizar el id respectivo.
		asigObject.add(libropackage.AsignaturaDAO.getAsignaturaByORMID(idAsignatura));
		// se setea el nombre del profesor en la ventana.
		jlnomprofe.setText(nombreProfesor);
		jlCurso.setText(curso.getNivel() + "� " + curso.getLetra());
		////////////////////////////////////////////////////////////////////
		// se inicializan las variables globales.
		idProfe = idprofesor;
		idAsign = idAsignatura;
		//System.out.println(idAsignatura);
		idCursoGlobal = idCurso;
		nombreP = nombreProfesor;
	}

	public void eliminarNota() {
		int opcion = JOptionPane.showConfirmDialog(null, "�Seguro que desea Ingresar?", "Advertencia",
				JOptionPane.YES_NO_OPTION);
		if (opcion == JOptionPane.YES_OPTION) {
			Conexion c = new Conexion();
			c.conectar();
			c.eliminar("nota", "id_pk", idNotasArray.get(cbNotas.getSelectedIndex()));
			c.desconectar();
			actualizar();
		}

	}

	public void editar() {
		try {
			libropackage.Nota notas = libropackage.NotaDAO.getNotaByORMID(idNotasArray.get(cbNotas.getSelectedIndex()));
			notas.setNota(Float.parseFloat(tfNotasEditar.getText()));
			// notas.setEstudiante_id_fk(notas.getEstudiante_id_fk());
			// notas.setAsignaturaid_pk(notas.getAsignaturaid_pk());
			cbNotas.removeAllItems();
			notasActuales();
		} catch (PersistentException e) {
			e.printStackTrace();
		}
	}

	public void notasActuales() {
		int index = cbEstudiantes.getSelectedIndex();
		try {
			libropackage.Estudiante est = libropackage.EstudianteDAO
					.getEstudianteByORMID(alumnoObject.get(index).getId_pk());
			cbNotas.removeAllItems();
			for (int i = 0; i < est.nota.toArray().length; i++) {
				if (est.nota.toArray()[i].getAsignaturaid_pk().getId_pk() == idAsign) {
					cbNotas.addItem(est.nota.toArray()[i].getNota());
					idNotasArray.add(est.nota.toArray()[i].getId_pk());
				}
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void insertarNota(JTextField jtnota) throws PersistentException {

		int index = cbEstudiantes.getSelectedIndex();
		//System.out.println(asigObject.get(0).getMateria());
		if (String.valueOf(cbEstudiantes.getSelectedItem())
				.equals(alumnoObject.get(index).getNombre() + " " + alumnoObject.get(index).getApellido())) {
			//System.out.println(alumnoObject.get(index).getNombre() + " " + alumnoObject.get(index).getApellido());
			libropackage.Nota nota = libropackage.NotaDAO.createNota();
			nota.setEstudiante_id_fk(alumnoObject.get(index));
			nota.setAsignaturaid_pk(asigObject.get(0));
			nota.setNota(Float.parseFloat(jtnota.getText()));
			libropackage.NotaDAO.save(nota);
		}

	}

	public void guardar() {
		try {
			int opcion = JOptionPane.showConfirmDialog(null, "�Seguro que desea Ingresar?", "Advertencia",
					JOptionPane.YES_NO_OPTION);
			if (opcion == JOptionPane.YES_OPTION) {
				insertarNota(tfnotaIngresar);
				actualizar();
			}

		} catch (PersistentException e1) {
			e1.printStackTrace();
		}
	}

	public void actualizar() {
		IngresarNotaGUI ia = new IngresarNotaGUI();
		ProfesorGUI pg = new ProfesorGUI();
		try {
			pg.profesor(idProfe);
			dispose();
			ia.estudiantes(idCursoGlobal, idProfe, idAsign, nombreP);
			ia.setVisible(true);
		} catch (PersistentException e1) {
			e1.printStackTrace();
		}
		dispose();
	}

}