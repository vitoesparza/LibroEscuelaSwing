package VentanasGUI.ProfesorGUIs;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;

import RunLESPackage.Conexion;
import VentanasGUI.ProfesorGUI;
import libropackage.CursoDAO;
import libropackage.ProfesorDAO;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;

public class IngresarPlanGUI extends JFrame {

	private JPanel contentPane;
	private JTextField jtfplan;
	private JLabel lbNombreProfesor = new JLabel("");
	private int idProfe;
	private int idAsign;
	private int idCursoGlobal;
	private JTextField tfPlaneditar;
	private libropackage.Profesor profesor;
	private libropackage.Asignatura asign;
	JComboBox cbPlanes = new JComboBox();
	ArrayList<Integer> idPlan = new ArrayList();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					IngresarPlanGUI frame = new IngresarPlanGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public IngresarPlanGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 350);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		lbNombreProfesor.setHorizontalAlignment(SwingConstants.CENTER);

		lbNombreProfesor.setBounds(43, 11, 342, 14);
		contentPane.add(lbNombreProfesor);

		JLabel lblIngresarPlanificacion = new JLabel("Ingresar Planificacion");
		lblIngresarPlanificacion.setHorizontalAlignment(SwingConstants.CENTER);
		lblIngresarPlanificacion.setBounds(43, 59, 342, 14);
		contentPane.add(lblIngresarPlanificacion);

		jtfplan = new JTextField();
		jtfplan.setBounds(43, 84, 342, 20);
		contentPane.add(jtfplan);
		jtfplan.setColumns(10);

		JButton btnIngresar = new JButton("Ingresar");
		btnIngresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ingresar();
			}
		});
		btnIngresar.setBounds(171, 115, 89, 23);
		contentPane.add(btnIngresar);

		JButton btnVolver = new JButton("Volver");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ProfesorGUI pg = new ProfesorGUI();
				try {
					pg.profesor(idProfe);
				} catch (PersistentException e1) {
					e1.printStackTrace();
				}
				pg.setVisible(true);
				dispose();
			}
		});
		btnVolver.setBounds(43, 277, 107, 23);
		contentPane.add(btnVolver);

		cbPlanes.setBounds(43, 184, 342, 20);
		contentPane.add(cbPlanes);

		cbPlanes.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					tfPlaneditar.setText(String.valueOf(cbPlanes.getSelectedItem()));
				}

			}
		});

		tfPlaneditar = new JTextField();
		tfPlaneditar.setBounds(43, 215, 342, 20);
		contentPane.add(tfPlaneditar);
		tfPlaneditar.setColumns(10);

		JButton btnEditar = new JButton("Editar");
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				editar();
			}
		});
		btnEditar.setBounds(278, 246, 107, 23);
		contentPane.add(btnEditar);

		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int opcion = JOptionPane.showConfirmDialog(null, "�Seguro que desea Eliminar?",
						"Advertencia",JOptionPane.YES_NO_OPTION);
				if(opcion==JOptionPane.YES_OPTION){
					eliminar();
					actualizar();
				}
			}
		});
		btnEliminar.setBounds(43, 246, 107, 23);
		contentPane.add(btnEliminar);

		JLabel lblEditarOEliminar = new JLabel("Editar o Eliminar Planificaci\u00F3n");
		lblEditarOEliminar.setHorizontalAlignment(SwingConstants.CENTER);
		lblEditarOEliminar.setBounds(43, 159, 342, 14);
		contentPane.add(lblEditarOEliminar);
	}

	public void planificacion(int idAsignatura, int idProfesor, int idCurso) throws PersistentException {
		libropackage.Curso curso = CursoDAO.getCursoByORMID(idCurso);
		profesor = ProfesorDAO.getProfesorByORMID(idProfesor);
		asign = libropackage.AsignaturaDAO.getAsignaturaByORMID(idAsignatura);
		lbNombreProfesor.setText(profesor.getNombre() + " " + profesor.getApellido() + ", Curso " + curso.getNivel()
				+ "� " + curso.getLetra() + ", Asignatura " + asign.getMateria());
		idProfe = idProfesor;
		idAsign = idAsignatura;
		idCursoGlobal=idCurso;
	}

	public void editPlan(int idProfesor, int idAsignatura) {
		try {
			profesor = libropackage.ProfesorDAO.getProfesorByORMID(idProfesor);
			asign = libropackage.AsignaturaDAO.getAsignaturaByORMID(idAsignatura);
			// System.out.println(asign.planificacion.toArray()[0].getActividad());
			for (int i = 0; i < asign.planificacion.toArray().length; i++) {
				cbPlanes.addItem(asign.planificacion.toArray()[i].getActividad());
				idPlan.add(asign.planificacion.toArray()[i].getId_pk());
			}
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void editar() {
		try {
			libropackage.Planificacion plan = libropackage.PlanificacionDAO
					.getPlanificacionByORMID(idPlan.get(cbPlanes.getSelectedIndex()));
			plan.setActividad(tfPlaneditar.getText());
			cbPlanes.removeAllItems();
			for (int i = 0; i < asign.planificacion.toArray().length; i++) {
				cbPlanes.addItem(asign.planificacion.toArray()[i].getActividad());
			}
		} catch (PersistentException e) {
			e.printStackTrace();
		}
	}

	public void eliminar() {
		Conexion c = new Conexion();
		c.conectar();
		c.eliminar("planificacion", "id_pk", idPlan.get(cbPlanes.getSelectedIndex()));
		c.desconectar();
		
	}
	
	public void ingresar() {
		int opcion = JOptionPane.showConfirmDialog(null, "�Seguro que desea Agregar?",
				"Advertencia",JOptionPane.YES_NO_OPTION);
		if(opcion==JOptionPane.YES_OPTION){
			guardar();
			actualizar();
		}
	}
	public void guardar(){
		libropackage.Asignatura asignatura;
		try {
			asignatura = libropackage.AsignaturaDAO.getAsignaturaByORMID(idAsign);
			libropackage.Planificacion plan = libropackage.PlanificacionDAO.createPlanificacion();
			plan.setAsignatura_id_fk(asignatura);
			plan.setActividad(jtfplan.getText());
			libropackage.PlanificacionDAO.save(plan);
			
		} catch (PersistentException e) {
			e.printStackTrace();
		}
		
	}
	
	public void actualizar(){
		IngresarPlanGUI ia = new IngresarPlanGUI();
		ProfesorGUI pg = new ProfesorGUI();
		try {
			pg.profesor(idProfe);
			dispose();
			ia.planificacion(idAsign, idProfe, idCursoGlobal);
			ia.editPlan(idProfe, idAsign);
			ia.setVisible(true);
		} catch (PersistentException e1) {
			e1.printStackTrace();
		}
		dispose();
	}
}
