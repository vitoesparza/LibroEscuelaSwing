package VentanasGUI.ProfesorGUIs;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.orm.PersistentException;

import RunLESPackage.Conexion;
import VentanasGUI.ProfesorGUI;
import libropackage.AnotacionesDAO;
import libropackage.CursoDAO;
import libropackage.Profesor;
import libropackage.ProfesorDAO;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class IngresarAnotacionGUI extends JFrame {

	private ArrayList<libropackage.Estudiante> alumnoObject = new ArrayList();
	ArrayList<Integer> idAnotacion = new ArrayList();
	private JLabel lbProfesor = new JLabel("");
	private JComboBox cbEstudiantes = new JComboBox();
	private JComboBox cbAnotaciones = new JComboBox();
	private JPanel contentPane;
	private JTextField tfAnotacion;
	private static final int ROW_COUNT = 1000;
	private int idProfe;
	private int idCursoLocal;
	private int idEstudiante;
	private JTextField tfEditar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					IngresarAnotacionGUI frame = new IngresarAnotacionGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public IngresarAnotacionGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 350);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		cbEstudiantes.setModel(new DefaultComboBoxModel(new String[] { "Seleccionar Estudiante" }));
		cbEstudiantes.setBounds(39, 30, 150, 20);
		contentPane.add(cbEstudiantes);

		tfAnotacion = new JTextField();
		tfAnotacion.setHorizontalAlignment(SwingConstants.CENTER);
		tfAnotacion.setBounds(41, 86, 351, 20);
		contentPane.add(tfAnotacion);
		tfAnotacion.setColumns(10);

		lbProfesor.setHorizontalAlignment(SwingConstants.CENTER);
		lbProfesor.setBounds(254, 30, 138, 14);
		contentPane.add(lbProfesor);

		JButton btnPositiva = new JButton("Positiva");
		btnPositiva.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int opcion = JOptionPane.showConfirmDialog(null, "�Seguro que desea Agregar Anotaci�n Positiva?", "Advertencia",
						JOptionPane.YES_NO_OPTION);
				if (opcion == JOptionPane.YES_OPTION) {
					insAnotacion(tfAnotacion, "+");
					actualizar();
				}
			}
		});
		btnPositiva.setBounds(303, 117, 89, 23);
		contentPane.add(btnPositiva);

		JButton btnNegativa = new JButton("Negativa");
		btnNegativa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int opcion = JOptionPane.showConfirmDialog(null, "�Seguro que desea Agregar Anotaci�n Negativa?", "Advertencia",
						JOptionPane.YES_NO_OPTION);
				if (opcion == JOptionPane.YES_OPTION) {
					insAnotacion(tfAnotacion, "-");
					actualizar();
				}
			}
		});
		btnNegativa.setBounds(41, 117, 89, 23);
		contentPane.add(btnNegativa);

		JButton btnVolver = new JButton("Volver");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ProfesorGUI pg = new ProfesorGUI();
				try {
					pg.profesor(idProfe);
				} catch (PersistentException e1) {
					e1.printStackTrace();
				}
				pg.setVisible(true);
				dispose();
			}
		});
		btnVolver.setBounds(41, 277, 106, 23);
		contentPane.add(btnVolver);

		tfEditar = new JTextField();
		tfEditar.setHorizontalAlignment(SwingConstants.CENTER);
		tfEditar.setColumns(10);
		tfEditar.setBounds(41, 207, 351, 20);
		contentPane.add(tfEditar);

		cbAnotaciones.setBounds(41, 176, 351, 20);
		contentPane.add(cbAnotaciones);

		cbEstudiantes.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					editAnotacion();
				}

			}
		});

		cbAnotaciones.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					String anotacion = String.valueOf(cbAnotaciones.getSelectedItem());
					tfEditar.setText(anotacion.substring(1, anotacion.length()));
				}

			}
		});

		JLabel lbbanner = new JLabel("Editar o Eliminar Anotaci\u00F3n");
		lbbanner.setHorizontalAlignment(SwingConstants.CENTER);
		lbbanner.setBounds(41, 151, 351, 14);
		contentPane.add(lbbanner);

		JButton btnEditar = new JButton("Editar");
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				editar();
			}
		});
		btnEditar.setBounds(284, 238, 108, 23);
		contentPane.add(btnEditar);

		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int opcion = JOptionPane.showConfirmDialog(null, "�Seguro que desea eliminar Anotaci�n?", "Advertencia",
						JOptionPane.YES_NO_OPTION);
				if (opcion == JOptionPane.YES_OPTION) {
					eliminarAnotacion();
					actualizar();
				}
			}
		});
		btnEliminar.setBounds(39, 238, 108, 23);
		contentPane.add(btnEliminar);

		JLabel lblIngresarAnotacin = new JLabel("Ingresar Anotaci\u00F3n");
		lblIngresarAnotacin.setHorizontalAlignment(SwingConstants.CENTER);
		lblIngresarAnotacin.setBounds(41, 61, 351, 14);
		contentPane.add(lblIngresarAnotacin);
	}

	public void anotacion(int idCurso, int idProfesor) throws PersistentException {
		libropackage.Curso curso = CursoDAO.getCursoByORMID(idCurso);
		libropackage.Profesor profesor = ProfesorDAO.getProfesorByORMID(idProfesor);
		libropackage.Estudiante[] alumnos = libropackage.EstudianteDAO.listEstudianteByQuery(null, null);
		int length = Math.min(alumnos.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			if (String.valueOf(alumnos[i].getCursoid_fk()).equals(String.valueOf(idCurso))) {
				cbEstudiantes.addItem(alumnos[i].getNombre() + " " + alumnos[i].getApellido());
				alumnoObject.add(alumnos[i]);
			}
		}
		lbProfesor.setText(
				profesor.getNombre() + " " + profesor.getApellido() + " " + curso.getNivel() + "� " + curso.getLetra());
		idCursoLocal = idCurso;
		idProfe = idProfesor;
	}

	public void insAnotacion(JTextField jtnota, String signo) {
		libropackage.Profesor profesor;
		try {
			profesor = ProfesorDAO.getProfesorByORMID(idProfe);
			int index = cbEstudiantes.getSelectedIndex() - 1;
			if (String.valueOf(cbEstudiantes.getSelectedItem())
					.equals(alumnoObject.get(index).getNombre() + " " + alumnoObject.get(index).getApellido())) {
				libropackage.Anotaciones anotacion = libropackage.AnotacionesDAO.createAnotaciones();
				anotacion.setProfesor_id_fk(profesor);
				anotacion.setEstudiante_id_fk(alumnoObject.get(index));
				anotacion.setAnotacion(signo + "" + jtnota.getText());
				libropackage.AnotacionesDAO.save(anotacion);
			}
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void editAnotacion() {
		int index = cbEstudiantes.getSelectedIndex() - 1;
		int id = alumnoObject.get(index).getId_pk();
		try {
			libropackage.Estudiante estudiante = libropackage.EstudianteDAO.getEstudianteByORMID(id);
			cbAnotaciones.removeAllItems();
			for (int i = 0; i < estudiante.anotaciones.toArray().length; i++) {
				cbAnotaciones.addItem(estudiante.anotaciones.toArray()[i].getAnotacion());
				idAnotacion.add(estudiante.anotaciones.toArray()[i].getId_pk());
			}

		} catch (PersistentException e) {
			e.printStackTrace();
		}
	}

	public void editar() {
		int id = idAnotacion.get(cbAnotaciones.getSelectedIndex());
		try {
			libropackage.Anotaciones anotacion = libropackage.AnotacionesDAO.getAnotacionesByORMID(id);
			int opcion = JOptionPane.showConfirmDialog(null, "�Desea que la anotacion sea Positiva?", "Advertencia",
					JOptionPane.YES_NO_OPTION);
			if (opcion == JOptionPane.YES_OPTION) {
				anotacion.setAnotacion("+" + tfEditar.getText());
			} else {
				anotacion.setAnotacion("-" + tfEditar.getText());
			}
			editAnotacion();
		} catch (PersistentException e) {
			e.printStackTrace();
		}
	}

	public void eliminarAnotacion() {
		Conexion c = new Conexion();
		c.conectar();
		c.eliminar("anotaciones", "id_pk", idAnotacion.get(cbAnotaciones.getSelectedIndex()));
		c.desconectar();
		editAnotacion();
	}

	public void actualizar() {
		IngresarAnotacionGUI ia = new IngresarAnotacionGUI();
		ProfesorGUI pg = new ProfesorGUI();
		try {
			pg.profesor(idProfe);
			dispose();
			ia.anotacion(idCursoLocal, idProfe);
			ia.setVisible(true);
		} catch (PersistentException e1) {
			e1.printStackTrace();
		}
		dispose();
	}
}