package VentanasGUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.xml.transform.TransformerException;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;

import Datos.reportes.apoderado.ReporteNotaAnotacionAp;
import Datos.reportes.apoderado.ReportePlanificaciones;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.DefaultComboBoxModel;

public class ApoderadoGUI extends JFrame {

	private JPanel contentPane;
	private JLabel lbApoderado = new JLabel("");
	private JComboBox cbPupilos = new JComboBox();
	private int idAp;
	private int idEst;
	private ArrayList <String>idEstList = new ArrayList();
	private ArrayList <String>idNotaList = new ArrayList();
	private ArrayList <String>idAnotacionList = new ArrayList();
	private boolean error = false;
	private static final int ROW_COUNT = 3000;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ApoderadoGUI frame = new ApoderadoGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ApoderadoGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnReporte = new JButton("Reporte");
		btnReporte.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					notasPorEst();
					JOptionPane.showMessageDialog(null, "Reporte Creado Exitosamente");
				} catch (PersistentException | TransformerException e) {
					JOptionPane.showMessageDialog(null, "Error");
					e.printStackTrace();
				}
			}
		});
		btnReporte.setBounds(140, 112, 171, 23);
		contentPane.add(btnReporte);
		cbPupilos.setModel(new DefaultComboBoxModel(new String[] {"Seleccione Estudiante"}));
		
		
		cbPupilos.setBounds(76, 58, 190, 20);
		contentPane.add(cbPupilos);
		
		JButton btnPlanificaciones = new JButton("Planificaciones");
		btnPlanificaciones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					planificacionesPorEst();
					JOptionPane.showMessageDialog(null, "Reporte de Planificaciones Creado Exitosamente");
				} catch (NumberFormatException | PersistentException | TransformerException e) {
					JOptionPane.showMessageDialog(null, "Error");
					e.printStackTrace();
				}
			}
		});
		btnPlanificaciones.setBounds(140, 148, 171, 23);
		contentPane.add(btnPlanificaciones);
		
		
		lbApoderado.setBounds(76, 11, 279, 14);
		contentPane.add(lbApoderado);
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				volver();
			}
		});
		btnVolver.setBounds(76, 212, 89, 23);
		contentPane.add(btnVolver);
	}
	
	public void apoderado(int idApoderado)throws PersistentException{
		try {
			PersistentTransaction t = libropackage.LibroClasePersistentManager.instance().getSession().beginTransaction();
			try {
				libropackage.Apoderado apoderado = libropackage.ApoderadoDAO.getApoderadoByORMID(idApoderado);
				lbApoderado.setText("Apoderado : "+apoderado.getNombre() +" "+ apoderado.getApellido());
				idAp=idApoderado;
				pupilos(idApoderado);
				t.commit();
			}
			catch (Exception e) {
				t.rollback();
				JOptionPane.showMessageDialog(null, "Apoderado no Existe");
				error=true;
			}
		}
		finally {
			libropackage.LibroClasePersistentManager.instance().disposePersistentManager();
		}
	}
	
	public void pupilos (int idApod) throws PersistentException{
		libropackage.Estudiante[] pupilo = libropackage.EstudianteDAO.listEstudianteByQuery(null, null);
		int length = Math.min(pupilo.length, ROW_COUNT);
		for (int i = 0; i < length; i++) {
			if(pupilo[i].getApoderado_id_fk().getId_pk()==idApod){
				cbPupilos.addItem(pupilo[i].getNombre()+" "+pupilo[i].getApellido()+" "
						+ ""+pupilo[i].getCursoid_fk().getNivel()+"� "+pupilo[i].getCursoid_fk().getLetra());
				idEstList.add(""+pupilo[i].getId_pk());
			}
		}
	}
	
	public void notasPorEst() throws PersistentException, TransformerException{
		ReporteNotaAnotacionAp rnaa = new ReporteNotaAnotacionAp();
		libropackage.Nota [] notas = libropackage.NotaDAO.listNotaByQuery(null, null);
		int tamNota = Math.min(notas.length, ROW_COUNT);
		for (int i = 0; i <tamNota; i++) {
			if(String.valueOf(notas[i].getEstudiante_id_fk()).equals(String.valueOf(idEstList.get(cbPupilos.getSelectedIndex()-1)))){
				idNotaList.add(""+notas[i].getId_pk());
			}
		}
		
		libropackage.Anotaciones[] anotacion = libropackage.AnotacionesDAO.listAnotacionesByQuery(null, null);
		int tamAnotacion = Math.min(anotacion.length, ROW_COUNT);
		for (int i = 0; i < tamAnotacion; i++) {
			if(anotacion[i].getEstudiante_id_fk().getId_pk()==Integer.parseInt(String.valueOf(idEstList.get(cbPupilos.getSelectedIndex()-1)))){
				idAnotacionList.add(""+anotacion[i].getId_pk());
			}
		}
		
		rnaa.generarDocument(idNotaList, idAnotacionList, idEstList, idAp);
		rnaa.generarXML();
	}
	
	public void planificacionesPorEst() throws NumberFormatException, PersistentException, TransformerException{
		ReportePlanificaciones rp = new ReportePlanificaciones();
		rp.generarDocument(idEstList, idAp);
		rp.generarXML();
	}
	
	public void volverByError(){
		if(error){
			Admin a = new Admin();
			a.setVisible(true);
			dispose();
		}
	}
	
	public void volver(){
		Admin a = new Admin();
		a.setVisible(true);
		dispose();
	}
}