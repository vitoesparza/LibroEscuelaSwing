package VentanasGUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.xml.transform.TransformerException;

import org.orm.PersistentException;

import Datos.Transformacion;
import Datos.reportes.colegio.AsistenciaPorEstudiante;
import Datos.reportes.colegio.AsistenciaXPE;
import Datos.reportes.colegio.EstudiantesReprobados;
import Datos.reportes.colegio.ReporteApVariosAlumnos;
import libroescuela.Apoderado;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.text.Normalizer;
import java.util.Set;
import java.util.TreeSet;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class ColegioGUI extends JFrame {

	private JPanel contentPane;
	private Set<String> apList = new TreeSet();
	private static final int ROW_COUNT = 1000;
	private JTextField TFporcentaje;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ColegioGUI frame = new ColegioGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ColegioGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnNewButton = new JButton("Apoderados");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					reporteApoderado();
				} catch (PersistentException | TransformerException e) {
					e.printStackTrace();
				}
			}
		});
		btnNewButton.setBounds(107, 172, 225, 23);
		contentPane.add(btnNewButton);

		JButton btnAsistencia = new JButton("Asistencias por estudiante");
		btnAsistencia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					asistencias();
				} catch (NumberFormatException | PersistentException | TransformerException e) {
					e.printStackTrace();
				}
			}

		});
		btnAsistencia.setBounds(107, 70, 225, 23);
		contentPane.add(btnAsistencia);

		JButton btnAlumnosBajoAsistencia = new JButton("Alumnos bajo Asistencia");
		btnAlumnosBajoAsistencia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					asistenciasPor();
				} catch (NumberFormatException | PersistentException | TransformerException e) {
					e.printStackTrace();
				}
			}
		});
		btnAlumnosBajoAsistencia.setBounds(107, 104, 171, 23);
		contentPane.add(btnAlumnosBajoAsistencia);

		JButton btnNewButton_1 = new JButton("Estudiantes Reprobados");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					reprobados();
				} catch (NumberFormatException | PersistentException | TransformerException e) {
					e.printStackTrace();
				}
			}
		});
		btnNewButton_1.setBounds(107, 138, 225, 23);
		contentPane.add(btnNewButton_1);

		JLabel lblInformacinGeneral = new JLabel("Informaci\u00F3n General");
		lblInformacinGeneral.setBounds(38, 25, 184, 14);
		contentPane.add(lblInformacinGeneral);
		
		TFporcentaje = new JTextField();
		TFporcentaje.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
//				try {
//					asistenciasPor();
//				} catch (NumberFormatException | PersistentException | TransformerException e) {
//					e.printStackTrace();
//				}
			}
		});
		TFporcentaje.setHorizontalAlignment(SwingConstants.CENTER);
		TFporcentaje.setBounds(288, 104, 42, 20);
		contentPane.add(TFporcentaje);
		TFporcentaje.setColumns(10);
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Admin a = new Admin();
				a.setVisible(true);
				dispose();
			}
		});
		btnVolver.setBounds(176, 227, 89, 23);
		contentPane.add(btnVolver);
	}

	private void reporteApoderado() throws PersistentException, TransformerException {
		ReporteApVariosAlumnos a = new ReporteApVariosAlumnos();
		Transformacion t = new Transformacion();
		libropackage.Estudiante[] estudiantes;
		estudiantes = libropackage.EstudianteDAO.listEstudianteByQuery(null, null);
		libropackage.Apoderado[] apoderado;
		apoderado = libropackage.ApoderadoDAO.listApoderadoByQuery(null, null);
		int tEst = Math.min(estudiantes.length, ROW_COUNT);
		int tApo = Math.min(apoderado.length, ROW_COUNT);
		int cont;
		for (int i = 0; i < tApo; i++) {
			cont = 0;
			for (int j = 0; j < tEst; j++) {
				if (apoderado[i].getId_pk() == Integer.parseInt(String.valueOf(estudiantes[j].getApoderado_id_fk()))) {
					cont++;
					if (cont > 1) {
						apList.add(String.valueOf(apoderado[i].getId_pk()));
					}
				}
			}
		}
		a.generarDocument(apList);
		a.generarXML();
		t.transformar("Apoderados_Varios_Alumnos.xslt", "Apoderados_Varios_Alumnos.xml", "Apoderados_Varios_AlumnosHTML.html");
		JOptionPane.showMessageDialog(null, "Reporte de apoderados creado con �xito");
	}
	

	private void asistencias() throws NumberFormatException, PersistentException, TransformerException {
		AsistenciaPorEstudiante ape = new AsistenciaPorEstudiante();
		ape.generarDocument();
		ape.generarXML();
		JOptionPane.showMessageDialog(null, "Reporte de Asistencias Creado con �xito");
	}

	private void asistenciasPor() throws NumberFormatException, PersistentException, TransformerException {
		int porcentaje;
		if(TFporcentaje.getText().equals("")){
			porcentaje=80;
		}else{
			porcentaje=Integer.parseInt(TFporcentaje.getText());
		}
		AsistenciaXPE axpe = new AsistenciaXPE();
		axpe.generarDocument(porcentaje);
		axpe.generarXML();
		JOptionPane.showMessageDialog(null, "Reporte de Asistencias Creado con �xito");
	}
	
	public void reprobados() throws NumberFormatException, PersistentException, TransformerException{
		EstudiantesReprobados er = new EstudiantesReprobados();
		er.generarDocument();
		er.generarXML();
		JOptionPane.showMessageDialog(null, "Reporte de Estudiantes Reprobados creado con �xito");
	}
}
