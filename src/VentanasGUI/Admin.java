package VentanasGUI;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;
import javax.swing.SwingConstants;

/**
 * este Jframe ser� el inicio donde se nos dar�n las opciones de elegir apoderado, profesor o curso
 * @author Ignacio Esparza
 *
 */
public class Admin extends JFrame {

	private ButtonGroup bg = new ButtonGroup();
	private JTextField jtid;
	private int num;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Admin frame = new Admin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Admin() {
		configuracion();
	}
	
	/**
	 * se inicializan los componentes
	 */
	public void configuracion() {

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(null);

		JRadioButton rdbtnApoderado = new JRadioButton("Apoderado");
		rdbtnApoderado.setBounds(69, 73, 109, 23);
		getContentPane().add(rdbtnApoderado);

		JRadioButton rdbtnProfesor = new JRadioButton("Profesor");
		rdbtnProfesor.setBounds(180, 73, 109, 23);
		getContentPane().add(rdbtnProfesor);

		JRadioButton rdbtnCurso = new JRadioButton("Colegio");
		rdbtnCurso.setBounds(291, 73, 109, 23);
		getContentPane().add(rdbtnCurso);

		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (rdbtnApoderado.isSelected()) {
					ApoderadoGUI ag = new ApoderadoGUI();
					try {
						num = Integer.parseInt(jtid.getText());
						ag.apoderado(num);
						ag.setVisible(true);
						dispose();
						ag.volverByError();
					} catch (PersistentException e) {
						e.printStackTrace();
					}
				}
				if (rdbtnProfesor.isSelected()) {
					num = Integer.parseInt(jtid.getText());
					ProfesorGUI pg = new ProfesorGUI();
					try {
						pg.profesor(num);
						pg.setVisible(true);
						dispose();
						pg.volverByError();

					} catch (PersistentException e) {
						e.printStackTrace();
					}
				}
				if (rdbtnCurso.isSelected()) {
					ColegioGUI cg = new ColegioGUI();
					cg.setVisible(true);
					dispose();
				}
			}
		});
		btnAceptar.setBounds(172, 184, 89, 23);
		getContentPane().add(btnAceptar);

		bg.add(rdbtnApoderado);
		bg.add(rdbtnProfesor);
		bg.add(rdbtnCurso);

		jtid = new JTextField();
		jtid.setHorizontalAlignment(SwingConstants.CENTER);
		jtid.setBounds(172, 153, 89, 20);
		getContentPane().add(jtid);
		jtid.setColumns(10);
		
		JButton btnOtrasOpciones = new JButton("Otras Opciones");
		btnOtrasOpciones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Opciones o = new Opciones();
				o.setVisible(true);
				dispose();
			}
		});
		btnOtrasOpciones.setBounds(10, 227, 129, 23);
		getContentPane().add(btnOtrasOpciones);
	}

}