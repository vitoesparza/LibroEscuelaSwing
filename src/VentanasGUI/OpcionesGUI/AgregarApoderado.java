package VentanasGUI.OpcionesGUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.orm.PersistentException;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AgregarApoderado extends JFrame {

	private JPanel contentPane;
	private JTextField tfNombre;
	private JTextField tfApellido;
	private JTextField tfRut;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AgregarApoderado frame = new AgregarApoderado();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AgregarApoderado() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnVolver = new JButton("Volver");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OpcionesApoderado oa = new OpcionesApoderado();
				oa.setVisible(true);
				dispose();
			}
		});
		btnVolver.setBounds(50, 212, 89, 23);
		contentPane.add(btnVolver);

		JButton btnAgregar = new JButton("Agregar");
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				agregarApo();
			}
		});
		btnAgregar.setBounds(291, 212, 89, 23);
		contentPane.add(btnAgregar);

		tfNombre = new JTextField();
		tfNombre.setBounds(204, 69, 176, 20);
		contentPane.add(tfNombre);
		tfNombre.setColumns(10);

		tfApellido = new JTextField();
		tfApellido.setColumns(10);
		tfApellido.setBounds(204, 100, 176, 20);
		contentPane.add(tfApellido);

		tfRut = new JTextField();
		tfRut.setColumns(10);
		tfRut.setBounds(204, 131, 176, 20);
		contentPane.add(tfRut);

		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(50, 72, 106, 14);
		contentPane.add(lblNombre);

		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(50, 103, 106, 14);
		contentPane.add(lblApellido);

		JLabel lblRut = new JLabel("Rut");
		lblRut.setBounds(50, 134, 106, 14);
		contentPane.add(lblRut);

		JLabel lblAgregarApoderado = new JLabel("Agregar Apoderado");
		lblAgregarApoderado.setHorizontalAlignment(SwingConstants.CENTER);
		lblAgregarApoderado.setBounds(0, 11, 434, 14);
		contentPane.add(lblAgregarApoderado);
	}

	public void agregarApo() {
		if (tfNombre.getText().equals("") || tfApellido.getText().equals("") || tfRut.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Falta llenar un campo,\nTodos los campos son Obligatorios.");
		} else {
			try {
				libropackage.Apoderado apoderado = libropackage.ApoderadoDAO.createApoderado();
				apoderado.setNombre(tfNombre.getText());
				apoderado.setApellido(tfApellido.getText());
				apoderado.setRut(tfRut.getText());
				libropackage.ApoderadoDAO.save(apoderado);
				JOptionPane.showMessageDialog(null, "Registrado con �xito");
			} catch (PersistentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
