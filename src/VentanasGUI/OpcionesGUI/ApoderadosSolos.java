package VentanasGUI.OpcionesGUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.orm.PersistentException;

import RunLESPackage.Conexion;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class ApoderadosSolos extends JFrame {

	private JPanel contentPane;
	private JComboBox cbApoderados = new JComboBox();
	private ArrayList<Integer> idApoderado = new ArrayList<>();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ApoderadosSolos frame = new ApoderadosSolos();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ApoderadosSolos() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		cbApoderados.setBounds(94, 73, 246, 20);
		contentPane.add(cbApoderados);

		JLabel lblApoderadosSinEstudiantes = new JLabel("Apoderados Sin Estudiantes");
		lblApoderadosSinEstudiantes.setHorizontalAlignment(SwingConstants.CENTER);
		lblApoderadosSinEstudiantes.setBounds(94, 29, 246, 14);
		contentPane.add(lblApoderadosSinEstudiantes);

		JButton btnEliminar = new JButton("Eliminar Seleccionado");
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int opcion = JOptionPane.showConfirmDialog(null, "�Seguro que desea eliminar Apoderado?", "Advertencia",
						JOptionPane.YES_NO_OPTION);
				if (opcion == JOptionPane.YES_OPTION) {
					eliminar();
					actualizar();
				}
			}
		});
		btnEliminar.setBounds(128, 143, 185, 23);
		contentPane.add(btnEliminar);

		JButton btnVolver = new JButton("Volver");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OpcionesApoderado oa = new OpcionesApoderado();
				oa.setVisible(true);
				dispose();
			}
		});
		btnVolver.setBounds(176, 212, 89, 23);
		contentPane.add(btnVolver);
	}

	public void listaApoderados() {
		libropackage.Apoderado[] apoderados;
		try {
			apoderados = libropackage.ApoderadoDAO.listApoderadoByQuery(null, null);
			int length = apoderados.length;
			for (int i = 0; i < length; i++) {
				if (apoderados[i].estudiante.toArray().length == 0) {
					//System.out.println(apoderados[i].getId_pk());
					cbApoderados.addItem(apoderados[i].getNombre() + " " + apoderados[i].getApellido());
					idApoderado.add(apoderados[i].getId_pk());
				}
			}
		} catch (PersistentException e) {
			e.printStackTrace();
		}
	}
	
	public void eliminarTodos(){
			libropackage.Apoderado[] apoderados;
			try {
				apoderados = libropackage.ApoderadoDAO.listApoderadoByQuery(null, null);
				int length = apoderados.length;
				for (int i = 0; i < length; i++) {
					if (apoderados[i].estudiante.toArray().length == 0) {
						eliminar();
					}
				}
			} catch (PersistentException e) {
				e.printStackTrace();
			}
		
	}

	public void eliminar() {
		Conexion c = new Conexion();
		c.conectar();
		c.eliminar("apoderado", "id_pk", idApoderado.get(cbApoderados.getSelectedIndex()));
		c.desconectar();
	}

	public void actualizar() {
		OpcionesApoderado oa = new OpcionesApoderado();
		dispose();
		ApoderadosSolos ea = new ApoderadosSolos();
		ea.listaApoderados();
		ea.setVisible(true);
		dispose();
	}

}
