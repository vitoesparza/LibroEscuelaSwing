package VentanasGUI.OpcionesGUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;

import com.mysql.fabric.xmlrpc.base.Array;

import RunLESPackage.Conexion;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class EditarAlumno extends JFrame {

	private JPanel contentPane;
	private JTextField tfNombre;
	private JTextField tfApellido;
	private JTextField tfRut;
	private JComboBox cbEstudiantes = new JComboBox();
	private JComboBox cbApoderado = new JComboBox();
	private JLabel lblCurso = new JLabel("");
	private ArrayList<Integer> idEstudiante = new ArrayList<>();
	private ArrayList<Integer> idApoderado = new ArrayList<>();
	private int idCurso;
	private libropackage.Curso curso;
	private JLabel lblNombreApoderado = new JLabel("Nombre Apoderado");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EditarAlumno frame = new EditarAlumno();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EditarAlumno() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 411);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		lblCurso.setHorizontalAlignment(SwingConstants.CENTER);
		lblCurso.setBounds(56, 23, 104, 14);
		contentPane.add(lblCurso);

		JButton btnEditar = new JButton("Editar");
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				editarEstudiante();
			}
		});
		btnEditar.setBounds(56, 309, 89, 23);
		contentPane.add(btnEditar);

		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int opcion = JOptionPane.showConfirmDialog(null, "�Seguro que desea eliminar Anotaci�n?", "Advertencia",
						JOptionPane.YES_NO_OPTION);
				if (opcion == JOptionPane.YES_OPTION) {
					eliminarEst();
					actualizar();
				}
			}
		});
		btnEliminar.setBounds(293, 309, 89, 23);
		contentPane.add(btnEliminar);

		cbEstudiantes.setBounds(56, 85, 321, 20);
		contentPane.add(cbEstudiantes);

		tfNombre = new JTextField();
		tfNombre.setBounds(162, 116, 215, 20);
		contentPane.add(tfNombre);
		tfNombre.setColumns(10);

		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(56, 116, 104, 14);
		contentPane.add(lblNombre);

		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(56, 147, 104, 14);
		contentPane.add(lblApellido);

		JLabel lblRut = new JLabel("Rut");
		lblRut.setBounds(56, 179, 104, 14);
		contentPane.add(lblRut);

		tfApellido = new JTextField();
		tfApellido.setColumns(10);
		tfApellido.setBounds(162, 145, 215, 20);
		contentPane.add(tfApellido);

		tfRut = new JTextField();
		tfRut.setColumns(10);
		tfRut.setBounds(162, 176, 215, 20);
		contentPane.add(tfRut);

		cbApoderado.setBounds(56, 278, 326, 20);
		contentPane.add(cbApoderado);

		JLabel lblApoderado = new JLabel("Seleccione Apoderado");
		lblApoderado.setHorizontalAlignment(SwingConstants.CENTER);
		lblApoderado.setBounds(56, 247, 326, 14);
		contentPane.add(lblApoderado);

		JButton btnVolver = new JButton("Volver");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				OpcionesAlumno oa = new OpcionesAlumno();
				oa.setVisible(true);
				dispose();
			}
		});
		btnVolver.setBounds(174, 338, 89, 23);
		contentPane.add(btnVolver);

		JLabel lblSeleccioneEstudiante = new JLabel("Seleccione Estudiante");
		lblSeleccioneEstudiante.setBounds(56, 60, 321, 14);
		contentPane.add(lblSeleccioneEstudiante);
		
		JLabel lblApoderadoActual = new JLabel("Apoderado Actual");
		lblApoderadoActual.setBounds(56, 210, 104, 14);
		contentPane.add(lblApoderadoActual);
		lblNombreApoderado.setHorizontalAlignment(SwingConstants.CENTER);
		
		
		lblNombreApoderado.setBounds(162, 210, 215, 14);
		contentPane.add(lblNombreApoderado);
	}

	public void editarAlumno(int id) {
		idCurso = id;
		try {
			curso = libropackage.CursoDAO.getCursoByORMID(id);
			lblCurso.setText("Curso " + curso.getNivel() + "� " + curso.getLetra());
			for (int i = 0; i < curso.estudiante.toArray().length; i++) {
				cbEstudiantes.addItem(curso.estudiante.toArray()[i].getNombre() + " " + curso.estudiante.toArray()[i].getApellido());
				idEstudiante.add(curso.estudiante.toArray()[i].getId_pk());
			}
			listener();
		} catch (PersistentException e) {
			e.printStackTrace();
		}
	}

	public void actualizarTf() {
		 try {
		 libropackage.Estudiante estLibro =
		 libropackage.EstudianteDAO.getEstudianteByORMID(idEstudiante.get(cbEstudiantes.getSelectedIndex()));
		 tfNombre.setText(estLibro.getNombre());
		 tfApellido.setText(estLibro.getApellido());
		 tfRut.setText(estLibro.getRut());
		 lblNombreApoderado.setText(estLibro.getApoderado_id_fk().getNombre()+" "
		 		+ ""+estLibro.getApoderado_id_fk().getApellido());
		 } catch (PersistentException e) {
		 // TODO Auto-generated catch block
		 e.printStackTrace();
		 }
	}
	
	public void listener(){
		cbEstudiantes.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					actualizarTf();
				}
			}
		});
	}
	
	public void apoderados(){
		libropackage.Apoderado[] apoderados;
		try {
			apoderados = libropackage.ApoderadoDAO.listApoderadoByQuery(null, null);
			for (int i = 0; i < apoderados.length; i++) {
				cbApoderado.addItem(apoderados[i].getNombre()+" "+apoderados[i].getApellido());
				idApoderado.add(apoderados[i].getId_pk());
			}
		} catch (PersistentException e) {
			e.printStackTrace();
		}
	}
	
	public void editarEstudiante(){
		try {
			PersistentTransaction t = libropackage.LibroClasePersistentManager.instance().getSession().beginTransaction();
			try {
				libropackage.Estudiante estudiante;
				estudiante = libropackage.EstudianteDAO.getEstudianteByORMID(idEstudiante.get(cbEstudiantes.getSelectedIndex()));
				libropackage.Apoderado apoderado;
				apoderado = libropackage.ApoderadoDAO.getApoderadoByORMID(idApoderado.get(cbApoderado.getSelectedIndex()));
				estudiante.setNombre(tfNombre.getText());
				estudiante.setApellido(tfApellido.getText());
				estudiante.setRut(tfRut.getText());
				estudiante.setApoderado_id_fk(apoderado);
				estudiante.setCursoid_fk(curso);
				libropackage.EstudianteDAO.save(estudiante);
				JOptionPane.showMessageDialog(null, "Editado con �xito");
				t.commit();
			} catch (PersistentException e) {
				e.printStackTrace();
			}
		} catch (PersistentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
	}
	
	public void eliminarEst(){
		Conexion c = new Conexion();
		c.conectar();
		c.eliminar("estudiante", "id_pk", idEstudiante.get(cbEstudiantes.getSelectedIndex()));
		c.desconectar();
	}
	
	public void actualizar(){
		OpcionesAlumno oa = new OpcionesAlumno();
		dispose();
		EditarAlumno ea = new EditarAlumno();
		ea.setVisible(true);
		ea.editarAlumno(idCurso);
		ea.apoderados();
		dispose();
	}
}
