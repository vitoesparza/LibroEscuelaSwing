package VentanasGUI.OpcionesGUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.orm.PersistentException;

import VentanasGUI.Opciones;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class MatriculaAlumno extends JFrame {

	private JPanel contentPane;
	private JTextField tfNombreEst;
	private JTextField tfApellidoEst;
	private JTextField tfRutEst;
	private JLabel lblCurso = new JLabel("Curso");
	private JComboBox cbApoderado = new JComboBox();
	private ArrayList<Integer> idApoderado = new ArrayList<>();
	private int idCurso;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MatriculaAlumno frame = new MatriculaAlumno();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MatriculaAlumno() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 376);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(20, 158, 46, 14);
		contentPane.add(lblNombre);

		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(20, 183, 46, 14);
		contentPane.add(lblApellido);

		JLabel lblRut = new JLabel("Rut");
		lblRut.setBounds(20, 208, 46, 14);
		contentPane.add(lblRut);

		cbApoderado.setBounds(257, 55, 143, 20);
		contentPane.add(cbApoderado);

		JLabel lblNewLabel = new JLabel("Seleccione Apoderado");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(257, 24, 143, 14);
		contentPane.add(lblNewLabel);

		tfNombreEst = new JTextField();
		tfNombreEst.setBounds(122, 155, 134, 20);
		contentPane.add(tfNombreEst);
		tfNombreEst.setColumns(10);

		JLabel lblEstudiante = new JLabel("Estudiante");
		lblEstudiante.setHorizontalAlignment(SwingConstants.CENTER);
		lblEstudiante.setBounds(58, 110, 97, 14);
		contentPane.add(lblEstudiante);

		tfApellidoEst = new JTextField();
		tfApellidoEst.setColumns(10);
		tfApellidoEst.setBounds(122, 180, 134, 20);
		contentPane.add(tfApellidoEst);

		tfRutEst = new JTextField();
		tfRutEst.setColumns(10);
		tfRutEst.setBounds(122, 205, 134, 20);
		contentPane.add(tfRutEst);

		JButton btnIngresarEstudiante = new JButton("Ingresar Estudiante");
		btnIngresarEstudiante.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				insertarEstudiante();
			}
		});
		btnIngresarEstudiante.setBounds(20, 259, 165, 23);
		contentPane.add(btnIngresarEstudiante);

		lblCurso.setHorizontalAlignment(SwingConstants.CENTER);
		lblCurso.setBounds(20, 24, 165, 14);
		contentPane.add(lblCurso);

		JButton btnVolver = new JButton("Volver");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				OpcionesAlumno oa = new OpcionesAlumno();
				oa.setVisible(true);
				dispose();
			}
		});
		btnVolver.setBounds(167, 303, 89, 23);
		contentPane.add(btnVolver);
	}

	public void cursoMostrar(int id) {
		idCurso = id;
		libropackage.Curso curso;
		try {
			curso = libropackage.CursoDAO.getCursoByORMID(id);
			lblCurso.setText("Curso " + curso.getNivel() + "� " + curso.getLetra());
			libropackage.Apoderado[] apoderados = libropackage.ApoderadoDAO.listApoderadoByQuery(null, null);
			for (int i = 0; i < apoderados.length; i++) {
				cbApoderado.addItem(apoderados[i].getNombre() + " " + apoderados[i].getApellido());
				idApoderado.add(apoderados[i].getId_pk());
			}
		} catch (PersistentException e) {
			e.printStackTrace();
		}
	}

//	public void actualizarApo() {
//
//		try {
//			cbApoderado.removeAllItems();
//			libropackage.Apoderado[] apoderados;
//			apoderados = libropackage.ApoderadoDAO.listApoderadoByQuery(null, null);
//			for (int i = 0; i < apoderados.length; i++) {
//				cbApoderado.addItem(apoderados[i].getNombre() + " " + apoderados[i].getApellido());
//				idApoderado.add(apoderados[i].getId_pk());
//			}
//		} catch (PersistentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//	}

	public void insertarEstudiante() {
		if (tfNombreEst.getText().equals("") || tfApellidoEst.getText().equals("") || tfRutEst.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Falta llenar un campo,\nTodos los campos son Obligatorios.");
		} else {

			try {
				libropackage.Estudiante estudiante = libropackage.EstudianteDAO.createEstudiante();
				libropackage.Curso curso = libropackage.CursoDAO.getCursoByORMID(idCurso);
				libropackage.Apoderado apoderado = libropackage.ApoderadoDAO
						.getApoderadoByORMID(idApoderado.get(cbApoderado.getSelectedIndex()));
				estudiante.setNombre(tfNombreEst.getText());
				estudiante.setApellido(tfApellidoEst.getText());
				estudiante.setRut(tfRutEst.getText());
				estudiante.setCursoid_fk(curso);
				estudiante.setApoderado_id_fk(apoderado);
				libropackage.EstudianteDAO.save(estudiante);
				JOptionPane.showMessageDialog(null, "Matriculado con �xito");
			} catch (PersistentException e) {
				e.printStackTrace();
			}
		}
	}

//	public void insertarApoderado() {
//		if (tfNombreApo.getText().equals("") || tfApellidoApo.getText().equals("") || tRutApo.getText().equals("")) {
//			JOptionPane.showMessageDialog(null, "Falta llenar un campo,\nTodos los campos son Obligatorios.");
//		} else {
//
//			try {
//				libropackage.Apoderado apoderado = libropackage.ApoderadoDAO.createApoderado();
//				apoderado.setNombre(tfNombreApo.getText());
//				apoderado.setApellido(tfApellidoApo.getText());
//				apoderado.setRut(tRutApo.getText());
//				libropackage.ApoderadoDAO.save(apoderado);
//				JOptionPane.showMessageDialog(null, "Ingresado con �xito");
//				actualizarApo();
//			} catch (PersistentException e) {
//				e.printStackTrace();
//			}
//		}
//	}
}
