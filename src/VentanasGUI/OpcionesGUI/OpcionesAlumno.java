package VentanasGUI.OpcionesGUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.orm.PersistentException;

import VentanasGUI.Opciones;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class OpcionesAlumno extends JFrame {

	private JPanel contentPane;
	private JComboBox cbCursosA = new JComboBox();
	JLabel lblSeleccioneCurso = new JLabel("Seleccione Curso");
	JLabel lblCSelect = new JLabel("");
	ArrayList<Integer> idCurso = new ArrayList<>();
	private final JButton btnNewButton = new JButton("Volver");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					OpcionesAlumno frame = new OpcionesAlumno();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public OpcionesAlumno() {
		
		cursos();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		cbCursosA.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					lblCSelect.setText("Curso Seleccionado " + cbCursosA.getSelectedItem());
				}

			}
		});
		
		cbCursosA.setBounds(38, 48, 149, 20);
		contentPane.add(cbCursosA);

		lblSeleccioneCurso.setBounds(38, 11, 149, 14);
		contentPane.add(lblSeleccioneCurso);

		JButton btnEditarEstudiante = new JButton("Editar/Eliminar Estudiante");
		btnEditarEstudiante.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EditarAlumno ea = new EditarAlumno();
				ea.setVisible(true);
				ea.editarAlumno(idCurso.get(cbCursosA.getSelectedIndex()));
				ea.apoderados();
				dispose();
			}
		});
		btnEditarEstudiante.setBounds(38, 169, 171, 23);
		contentPane.add(btnEditarEstudiante);

		JButton btnMatricular = new JButton("Matricular");
		btnMatricular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MatriculaAlumno ma = new MatriculaAlumno();
				ma.setVisible(true);
				ma.cursoMostrar(idCurso.get(cbCursosA.getSelectedIndex()));
				dispose();
			}
		});
		btnMatricular.setBounds(215, 169, 171, 23);
		contentPane.add(btnMatricular);

		lblCSelect.setBounds(237, 11, 149, 14);
		contentPane.add(lblCSelect);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Opciones o = new Opciones();
				o.setVisible(true);
				dispose();
			}
		});
		btnNewButton.setBounds(173, 225, 89, 23);
		
		contentPane.add(btnNewButton);
	}

	public void cursos() {
		try {
			libropackage.Curso[] cursos = libropackage.CursoDAO.listCursoByQuery(null, null);
			for (int i = 0; i < cursos.length; i++) {
				cbCursosA.addItem(cursos[i].getNivel() + "� " + cursos[i].getLetra());
				idCurso.add(cursos[i].getId_pk());
			}
		} catch (PersistentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// cbCursosA.addItem(arg0);
	}

}
