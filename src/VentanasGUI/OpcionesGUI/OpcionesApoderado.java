package VentanasGUI.OpcionesGUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import VentanasGUI.Opciones;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class OpcionesApoderado extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					OpcionesApoderado frame = new OpcionesApoderado();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public OpcionesApoderado() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("Editar/Eliminar Apoderado");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EditarApoderado frame = new EditarApoderado();
				frame.listaApoderados();
				frame.setVisible(true);
				dispose();
			}
		});
		btnNewButton.setBounds(113, 108, 208, 23);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Agregar");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AgregarApoderado frame = new AgregarApoderado();
				frame.setVisible(true);
				dispose();
			}
		});
		btnNewButton_1.setBounds(113, 74, 208, 23);
		contentPane.add(btnNewButton_1);
		
		JButton btnApoderadosSinEstudiantes = new JButton("Apoderados Sin Estudiantes");
		btnApoderadosSinEstudiantes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ApoderadosSolos frame = new ApoderadosSolos();
				frame.listaApoderados();
				frame.setVisible(true);
				dispose();
			}
		});
		btnApoderadosSinEstudiantes.setBounds(113, 144, 208, 23);
		contentPane.add(btnApoderadosSinEstudiantes);
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Opciones o = new Opciones();
				o.setVisible(true);
				dispose();
			}
		});
		btnVolver.setBounds(175, 227, 89, 23);
		contentPane.add(btnVolver);
		
		JLabel lblOpcionesApoderado = new JLabel("Opciones Apoderado");
		lblOpcionesApoderado.setHorizontalAlignment(SwingConstants.CENTER);
		lblOpcionesApoderado.setBounds(113, 11, 208, 14);
		contentPane.add(lblOpcionesApoderado);
	}
}
