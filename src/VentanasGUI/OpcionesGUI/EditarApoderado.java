package VentanasGUI.OpcionesGUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;

import com.mysql.fabric.xmlrpc.base.Array;

import RunLESPackage.Conexion;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class EditarApoderado extends JFrame {

	private JPanel contentPane;
	private JTextField tfNombre;
	private JTextField tfApellido;
	private JTextField tfRut;
	private JComboBox cbApoderados = new JComboBox();
	private ArrayList<Integer> idApoderado = new ArrayList<>();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EditarApoderado frame = new EditarApoderado();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EditarApoderado() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 383);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		cbApoderados.setBounds(68, 50, 304, 20);
		contentPane.add(cbApoderados);

		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(68, 115, 126, 14);
		contentPane.add(lblNombre);

		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(68, 147, 126, 14);
		contentPane.add(lblApellido);

		JLabel lblRut = new JLabel("Rut");
		lblRut.setBounds(68, 178, 126, 14);
		contentPane.add(lblRut);

		tfNombre = new JTextField();
		tfNombre.setBounds(192, 112, 180, 20);
		contentPane.add(tfNombre);
		tfNombre.setColumns(10);

		tfApellido = new JTextField();
		tfApellido.setBounds(192, 144, 180, 20);
		contentPane.add(tfApellido);
		tfApellido.setColumns(10);

		tfRut = new JTextField();
		tfRut.setBounds(192, 175, 180, 20);
		contentPane.add(tfRut);
		tfRut.setColumns(10);

		JButton btnEditar = new JButton("Editar");
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				editar();
				actualizar();
			}
		});
		btnEditar.setBounds(283, 244, 89, 23);
		contentPane.add(btnEditar);

		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int opcion = JOptionPane.showConfirmDialog(null, "�Seguro que desea eliminar Apoderado?", "Advertencia",
						JOptionPane.YES_NO_OPTION);
				if (opcion == JOptionPane.YES_OPTION) {
					eliminar();
					actualizar();
				}
			}
		});
		btnEliminar.setBounds(68, 244, 89, 23);
		contentPane.add(btnEliminar);

		JButton btnVolver = new JButton("Volver");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				OpcionesApoderado oa = new OpcionesApoderado();
				oa.setVisible(true);
				dispose();
			}
		});
		btnVolver.setBounds(175, 296, 89, 23);
		contentPane.add(btnVolver);
	}
	
	public void editar(){
		try {
			PersistentTransaction t = libropackage.LibroClasePersistentManager.instance().getSession().beginTransaction();
			try {
				libropackage.Apoderado apoderado;
				apoderado = libropackage.ApoderadoDAO.getApoderadoByORMID(idApoderado.get(cbApoderados.getSelectedIndex()));
				apoderado.setNombre(tfNombre.getText());
				apoderado.setApellido(tfApellido.getText());
				apoderado.setRut(tfRut.getText());
				libropackage.ApoderadoDAO.save(apoderado);
				JOptionPane.showMessageDialog(null, "Editado con �xito");
				t.commit();
			} catch (PersistentException e) {
				e.printStackTrace();
			}
		} catch (PersistentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public void listaApoderados() {
		libropackage.Apoderado[] apoderados;
		try {
			apoderados = libropackage.ApoderadoDAO.listApoderadoByQuery(null, null);
			int length = apoderados.length;
			for (int i = 0; i < length; i++) {
				cbApoderados.addItem(apoderados[i].getNombre() + " " + apoderados[i].getApellido());
				idApoderado.add(apoderados[i].getId_pk());
			}
			listener();
		} catch (PersistentException e) {
			e.printStackTrace();
		}

	}

	public void actualizarTf() {
		try {
			libropackage.Apoderado apo = libropackage.ApoderadoDAO.getApoderadoByORMID(idApoderado.get(cbApoderados.getSelectedIndex()));
			tfNombre.setText(apo.getNombre());
			tfApellido.setText(apo.getApellido());
			tfRut.setText(apo.getRut());
		} catch (PersistentException e) {
			e.printStackTrace();
		}
	}

	public void listener() {
		cbApoderados.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					actualizarTf();
				}
			}
		});
	}
	
	public void eliminar(){
		Conexion c = new Conexion();
		c.conectar();
		c.eliminar("apoderado", "id_pk", idApoderado.get(cbApoderados.getSelectedIndex()));
		c.desconectar();
	}
	
	public void actualizar(){
		OpcionesApoderado oa = new OpcionesApoderado();
		dispose();
		EditarApoderado ea = new EditarApoderado();
		ea.listaApoderados();
		ea.setVisible(true);
		dispose();
	}

}
