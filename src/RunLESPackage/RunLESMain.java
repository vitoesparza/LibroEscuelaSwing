package RunLESPackage;

import VentanasGUI.Admin;

/**
 * @version 1.11.60
 * @author Ignacio Esparza
 *
 */
public class RunLESMain {

	public static void main(String[] args) {
		Admin a = new Admin();
		a.setVisible(true);
	}

}
