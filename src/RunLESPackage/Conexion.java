package RunLESPackage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Conexion {
	private Connection con = null;
    private String database;

    public Conexion() {
        this.database = "libroescuela";
    }

    public void conectar() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = null;
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + database, "root", "");
            //System.out.println("Conexion Exitosa");
        } catch (Exception e) {
            System.out.println("Error" + e);
        }
    }

    public void desconectar() {
        if (con != null) {
            try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
        }
    }

   

    public void eliminar(String tabla, String columna, int condicion) {
        try {
            Statement st = con.createStatement();
            String sql;
            sql = "DELETE FROM "+tabla+" WHERE " +columna+" = "+condicion;
            st.executeUpdate(sql);

        } catch (SQLException e) {
            e.getMessage();
        }

    }

}
